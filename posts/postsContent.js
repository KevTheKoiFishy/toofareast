var content =
[
  
  {
   type:     "Article Snippet",
   title:    "Hong Kong Police Arrests 2 for Splashing Water at them During Water-Splashing Festival",
   subtitle: "Was it political unrest or no sense of humor?",
   date:     "April 23 2023",
   image:    "https://hongkongfp.com/wp-content/uploads/2023/04/Kyle_HKFP_20230409_SetA_7-Copy.jpg",
   para:     'During an event celebrating Songkran, two 20-year-old young men were arrested for shooting water guns at police. They were considered as attacking law enforcers in the cover of celebrating festivals. The celebration event was held in Kowloon City, a district where many Thais live. Songkran is the Thai New Year when people use water to drive away bad luck in the new year. However, this time, since the target was the police, traditions that represent blessing are considered disrespectful.',
   buttons:  [["Read Full Article", "/posts/A4_SongkranPoliceIncident.html"]],
  },
  
  {
   type:     "Article Snippet",
   title:    "The Book Rescuers",
   subtitle: "How Taiwanese Researchers are Reviving An Ancient Text",
   date:     "April 16 2023",
   image:    "https://imgcdn.cna.com.tw/www/WebPhotos/1024/20230409/1024x768_wmkn_0_C20230409000097.jpg",
   para:     'Islam is a precious and rare religion in Taiwan, making the recent accomplishment of The Taiwan National Library significant to the development of Taiwanese culture. On April 9th, Sean Scanaln from the Taiwan News reported that a 500-year-old handwritten Quran had been miraculously repaired by Hsu Mei-Wen-a, book restorer from The Taiwan National Library. The ancient Arabic language connects the Hui with their ancestors’ wisdom.',
   buttons:  [["Read Full Article", "/posts/A2_TheBookRescuers.html"]],
  },
  
  {
   type:     "Article Snippet",
   title:    "Filling a Bucket, or Lighting a Fire?",
   subtitle: "The world's manufacturing superpower runs its schools like factories.",
   date:     "April 16 2023",
   image:    "https://www.icecusa.org/wp-content/uploads/2015/05/Chinese_Classroom-1000x651.jpg",
   para:     'Just a few decades ago, China’s slow economic and social growth correlates strongly with the illiteracy permeating through the mass population, which gives a strong contrast to China’s current situation. The question is, what was the price tag for China\'s success, and what are the possible consequences?',
   buttons:  [["Read Full Article", "/posts/A1_ChineseEducationSystem.html"]],
  },
  
  {
   type:     "Article Snippet",
   title:    "Thailand's Massive Air Pollution Problem",
   subtitle: "This is the Lorax In Real Life.",
   date:     "April 2 2023",
   image:    "https://static.bangkokpost.com/media/content/dcx/2021/02/10/3890255.jpg",
   para:     'Over the past few weeks, 53 of Thailand’s 77 provinces’ air quality has plummeted to disturbing levels.\nThe Environmental Protection Agency (EPA) designates healthy PM 2.5 levels at 12 ug/m3 or lower. Thailand’s capital Bangkok had ranges between 55.6 µg/m³ and 81.4 µg/m³ consecutively from March 6th to 10th.\nReports from the year 2021 show that air pollution contributed to over 50,000 premature deaths in Thailand.',
   buttons:  [["Read Full Article", "/posts/A0_ThailandAirPollution.html"]],
  },
  
  {
   type:     "Article Snippet",
   title:    "Hollywood - A Beacon of Hope",
   subtitle: "\"I spent a year in a refugee camp. And somehow, I ended up here on Hollywood’s biggest stage.\"",
   date:     "Mar 24 2023",
   image:    "https://media.npr.org/assets/img/2023/03/14/ap23072332548106-a161eeb29ec639b6efe15cb5a35b6f7288956a9e-s1100-c50.jpg",
   para:     'In Oscars 93 years of history, no Asian female has ever received the Best Actress Award. However, the living legend Michelle Yeoh herself, makes this historic victory of becoming the first Asian female Best Actress Award winner. The movie “Every Everywhere All at Once” highlights her outstanding performance. Moreover, this masterpiece also marked a miracle for Chinese Actor, Ke Huy Quan, winning his first Oscars marks an important success in his film career.',
   buttons:  [["Read Full Article", "/posts/9_AsianOscarMilestone.html"]],
  },
  
  {
   type:     "Article Snippet",
   title:    "South Korea's Power Problem",
   subtitle: "Here's How to Get Away with Assault",
   date:     "Mar 12 2023",
   image:    "https://ca-times.brightspotcdn.com/dims4/default/f56ce56/2147483647/strip/false/crop/2047x1151+0+0/resize/1486x836!/quality/80/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F90%2Fe6%2F12c59975666de319e243b5f90885%2Fla-1550796207-tfn6tutu8y-snap-image",
   para:     'In July of 2021, 4 South Korean middle school students physically and sexually assaulted a fellow middle school girl with various objects for almost 2 hours. Her crime? Answering a phone call. A circulating picture of the 13-year old victim’s bloodied body caused immense public outrage; however, due to South Korea’s Juvenile Protection Act exempting children aged 10 to 14 from criminal punishment, the four perpetrators were released with minimal sentencing...',
   buttons:  [["Read Full Article", "/posts/8_SouthKoreaPowerProblem.html"]],
  },
  
  {
   type:     "Article Snippet",
   title:    "China's Economy is Back on Track",
   subtitle: "Opening up after Zero-Covid Pays Off",
   date:     "Feb 19 2023",
   image:    "https://media.wired.com/photos/5d4a23afe89e960009876418/16:9/w_2400,h_1350,c_limit/business_yuan_815649102.jpg",
   para:     'After China Dropped its Zero Covid policies, there was a tantrum of covid patients crowding up the hospital and doctors working overtime to take care of everyone. However, by now, things are settling, and the environment is beginning to go back to pre-covid season. This is evident in everything from the nostalgic traffic jams to the overflowing subway stations in Beijing and Shanghai...',
   buttons:  [["Read Full Article", "/posts/7_ChinaEconomyRevitalized.html"]],
  },
  
  {
   type:     "Article Snippet",
   title:    "The Hong Kong Housing Crisis",
   subtitle: "This is why 200,000 people live in homes smaller than a car.",
   date:     "Feb 10 2023",
   image:    "https://cdn.theatlantic.com/thumbor/l-rjVfqYSX_IfJVDT9zxGlFRdk4=/900x600/media/img/photo/2017/05/the-coffin-homes-of-hong-kong/h10_AP17127167017602/original.jpg",
   para:     'Imagine being forced to live in spaces smaller than an average parking space. A flat surrounded by thin walls, low ceilings, and little to no light. For many Hong Kongers, this is the affordable housing accommodation they could find.<br>The lack of affordable housing and government-subsidized public estates has forced many of Hong Kong’s residents to live in tight spaces, many of them having to cram into subdivided flats or cage homes, where a normal flat is shared with many people. This is the Hong Kong housing crisis. ',
   buttons:  [["Read Full Article", "/posts/6_HongKongHousingCrisis.html"]],
  },
  
  {
   type:     "Article Snippet",
   title:    "More Than the Majority",
   subtitle: "Lunar New Year isn't just what happens in the Middle Kingdom",
   date:     "Jan 29 2023",
   image:    "https://i.pinimg.com/originals/4e/6d/0b/4e6d0bbba27b5f2dc1daa33e4f3c47f7.jpg",
   para:     'Although colloquially known as “Chinese New Year”, Lunar New Year celebrations and traditions vary depending on the culture one is from: in Vietnam, children may receive li xi, small red envelopes filled with money, from family and friends; similarly, Korean children learn elaborate bows, called “sebae”...',
   buttons:  [["Read Full Article", "/posts/5_MoreThanTheMajority.html"]],
  },
  
  {
   type:     "Article Snippet",
   title:    "The Long Fight to Democracy",
   subtitle: "What can we learn about the Myanmar coup through the Gwangju uprising?",
   date:     "Jan 19 2023",
   image:    "https://www.marxist.com/images/stories/south_korea/2022_May/mourning_Image_May_18_Democratic_Uprising_Archive.jpg",
   para:     'Does everyone remember the Myanmar coup in 2021? The Myanmar coup d’état began on the morning of February 1st 2021. The National League for Democracy, the democratically elected leadership of the country, was deposed by the Tatmadaw, Myanmar’s military...<br>On the 16th of May 1961, Park Chung-hee and his allies took over the Korean government. Their dictatorship lasted twenty-seven years...',
   buttons:  [["Read Full Article", "/posts/4_LongFightToDemocracy.html"]],
  },
  
  {
   type:     "Article Snippet",
   title:    "Iron Fist to Let it Rip",
   subtitle: "Why China completely FLIPPED its COVID policy in just One Month",
   date:     "Jan 11 2023",
   image:    "https://assets.bwbx.io/images/users/iqjWHBFdfxIU/iRR2kxsEz1X0/v0/1200x-1.jpg",
   para:     'January 8th marks the end of all COVID restrictions. The 387 passengers flying from Singapore and Toronto stepped onto the mainland without a PCR test, without a three week hotel quarantine.<br>But the sudden change in policy has left hospitals overwhelmed, medication stores emptied, and crematorium lines overflowing...',
   buttons:  [["Read Full Article", "/posts/3_LetItRip.html"]],
  },
  
  {
   type:     "Article Snippet",
   title:    "The Real Tragedy Wasn't The Virus",
   subtitle: "A Review of China's Zero-Tolerance Days",
   date:     "Jan 3 2023",
   image:    "https://static.independent.co.uk/2022/10/19/11/2022-10-14T110118Z_1390418572_RC2Y0X9TSB0U_RTRMADP_3_CHINA-CONGRESS-PROTESTS-BEIJING-UGC.JPG",
   para:     'Written by an anonymous "Bridge Man," this October 13 poster encapsulates Chinese citizens\' growing dissent towards Xi and the Zero-COVID policy. While the western world has been accused of prioritizing economic profit over public health, China has done the opposite extreme...',
   buttons:  [["Read Full Article", "/posts/2_ChinaZeroTolerance.html"]],
  },
  
  {
   type:     "Article Snippet",
   title:    "180 Rohingya Refugees Presumed Dead at Sea",
   subtitle: "The Consequence of a Much Larger Issue",
   date:     "Dec 27 2022",
   image:    "https://www.amnesty.org/en/wp-content/uploads/2021/06/rohingya-refugees-1444x710.jpg",
   para:     'Earlier this week, a boat carrying over 100 Rohignya refugees disappeared at sea. The boat, which departed from Bangladesh, was most likely on its way to a Southeast Asian country. Although tragic...<br>The Rohingya are the people of a stateless, Indo-Aryan ethnic tribe who have historically resided in Myanmar. A Muslim tribe and an ethnic minority...',
   buttons:  [["Read Post", "/posts/1_180RohingyaDead.html"]],
  },
  
  {
   type:     "Article Snippet",
   title:    "South Koreans Are All Soon Becoming Younger?",
   subtitle: "Age = Age - 1;",
   date:     "Dec 20 2022",
   image:    "https://cdn.shopify.com/s/files/1/0185/3988/files/Screenshot_2020-12-03_at_10.21.08_1024x1024.png",
   para:     'On December 15th, 2022, South Korea passed a law that would adopt the international standard of age counting. South Korean used to have a traditional age system that when everyone is born at the age of 1 and will grow older every New Year. This previous age system has caused a huge amount of confusion...',
   buttons:  [["Read Post", "/posts/0_SKYounger.html"]],
  },
  
];